import {MongoClient} from 'mongodb'

const URL = 'mongodb://chat_10user:chat_10user@ds125263.mlab.com:25263/chat10_db';


export default class Database{

	connect(){


		return new Promise((resolve, reject) => {

			MongoClient.connect(URL, (err, db) => {
				
				return err ? reject(err) : resolve(db);

			});


		});



	}
}